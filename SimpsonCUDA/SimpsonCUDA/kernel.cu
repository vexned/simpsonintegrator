
#include <cuda.h>
#include <cuda_runtime_api.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

#include <stdio.h>
#include <chrono>
#include <iostream>


#define BLOCK_SIZE 256 
#define GRID_SIZE 32
//#define BLOCK_SIZE 	1024	
//#define	N			(16*256*256)	
//#define	N2			134217728 //2^27	
cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);
cudaError_t Simpson(double a, double b, double *c, int n);


__global__ void reduce5(double * inData, double * outData)
{
	__shared__ int data[BLOCK_SIZE];
	int	  tid = threadIdx.x;
	int	  i = 2 * blockIdx.x * blockDim.x + threadIdx.x;

	data[tid] = inData[i] + inData[i + blockDim.x];		// load into shared memeory

	__syncthreads();

	for (int s = blockDim.x / 2; s > 32; s >>= 1)
	{
		if (tid < s)
			data[tid] += data[tid + s];

		__syncthreads();
	}

	if (tid < 32)					// unroll last iterations
	{
		data[tid] += data[tid + 32];
		data[tid] += data[tid + 16];
		data[tid] += data[tid + 8];
		data[tid] += data[tid + 4];
		data[tid] += data[tid + 2];
		data[tid] += data[tid + 1];
	}

	if (tid == 0)					// write result of block reduction
		outData[blockIdx.x] = data[0];
}

//5.2 12.0
__device__ float f1(float x) {
	return ((x + 1)*(x + 1)) / sqrt(log(x));
}


//3.0 7.2
__device__ float f2(float x) {
	return sqrt(x) * exp(-x / 2);
}


//1.2 5.3
__device__ float f3(float x) {
	return ((1 + x)*(1 + x)) / (x*x*x*sqrt(2 + x));
}


__global__ void integrateKernelf1(float a, float h, int n, float* result, int ThreadStride, int resStride) {
	__shared__ float data[BLOCK_SIZE];
	int i = threadIdx.x + blockIdx.x * BLOCK_SIZE + ThreadStride + 1;
	if (i > n) return;
	if (i & 1)
	{
		data[threadIdx.x] = 4 * f1(a + i*h);
	}
	else
	{
		data[threadIdx.x] = 2 * f1(a + i*h);
	}
	if(threadIdx.x != 0)
		data[0] += data[threadIdx.x];
	__syncthreads();
	result[blockIdx.x + resStride] = data[0];
}
__global__ void integratef1Kernel (double *c, double a, double b, double h, int n)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	double x = a + (double)idx * h;
	if (idx == 0) {
		c[idx] = f1(x);
		return;
	}
	
	if (idx & 1)
	{
		c[idx] = 4 * f1(x);
		return;
	}
	else
	{
		c[idx] = 2 * f1(x);
		return;
	}
	if (idx == n)
		c[idx] = f1(b);
}

__global__ void integratef2Kernel(float *a, float c, float d, float deltaX, int n)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	float x = c + (float)idx * deltaX;
	if (idx == 0) {
		a[idx] = f2(x);
		return;
	}

	if (idx & 1)
	{
		a[idx] = 4 * f2(x);
		return;
	}
	else
	{
		a[idx] = 2 * f2(x);
		return;
	}
	if (idx == n)
		a[idx] = f2(d);
}

//__host__ double cudaIntegrate(double a, double b)
//{
//	// deltaX
//	double * b4 = new double[N2];
//	int n = N2;
//	double       * adev[2] = { NULL, NULL };
//	cudaMalloc((void**)&adev[0], sizeof(double)*N2);
//	cudaMalloc((void**)&adev[1], sizeof(double)*N2);
//	double h = (b - a) / N2;
//
//	// error code variable
//	cudaError_t errorcode = cudaSuccess;
//
//	// do calculation on device
//	int n_blocks = N2 / BLOCK_SIZE + (N % BLOCK_SIZE == 0 ? 0 : 1);
//	// cout << "blocks: " << n_blocks << endl;
//	// cout << "block size: " << block_size << endl;
//
//	integratef1Kernel << < n_blocks, BLOCK_SIZE >> > (adev[0], a, b, h, N2);
//	int i;
//
//	//for (i = 0; n >= BLOCK_SIZE; n /= (2 * BLOCK_SIZE), i ^= 1)
//	//{
//	//	// set kernel launch configuration
//	//	dim3 dimBlock(BLOCK_SIZE, 1, 1);
//	//	dim3 dimGrid(n / (2 * dimBlock.x), 1, 1);
//
//	//	reduce5 << <dimGrid, dimBlock >> > (adev[i], adev[i ^ 1]);
//	//}
//
//	cudaMemcpy(b4, adev[0], sizeof(double)* N2, cudaMemcpyDeviceToHost);
//	for (i = 1; i < n; i++)
//		b4[0] += b4[i];
//	double sum = b4[0];
//	cudaFree(adev[0]);
//	cudaFree(adev[1]);
//	// clean up
//	delete[] b4;
//
//	return sum;
//}

__host__ float integrateWithCuda(float a, float b, int N) {
	float *res;
	float sum = 0;
	float h = (b - a) / N;
	if (cudaMalloc((void**)&res, sizeof(float)*GRID_SIZE*BLOCK_SIZE) != cudaSuccess)
		return -1;

	int iNum = ceil((float)N / BLOCK_SIZE*GRID_SIZE);
	for (int i = 0; i < iNum; i++)
	{
		integrateKernelf1 << <BLOCK_SIZE, GRID_SIZE >> > (a,h, N, res, i*GRID_SIZE*BLOCK_SIZE, i*GRID_SIZE);
	}

	float *res2 = (float*)malloc(sizeof(float)*GRID_SIZE*BLOCK_SIZE);
	cudaMemcpy(res, res2, sizeof(float)*GRID_SIZE*BLOCK_SIZE, cudaMemcpyHostToDevice);
	
	for (int i = 0; i < GRID_SIZE*BLOCK_SIZE; i++)
		std::cout << res2[i];

	int k;
}



__global__ void addKernel(int *c, const int *a, const int *b)
{
    int i = threadIdx.x;
    c[i] = a[i] + b[i];
}

int main()
{
	//auto t1 = std::chrono::high_resolution_clock::now();
	double answer = integrateWithCuda(5.2, 12.0, 100000);
	//auto t2 = std::chrono::high_resolution_clock::now();
	//std::cout << "f1 took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		//<< " milliseconds. result = " << answer << std::endl;

	//std::cout << "Answer: " << answer;
	system("pause");
    //const int arraySize = 5;
    //const int a[arraySize] = { 1, 2, 3, 4, 5 };
    //const int b[arraySize] = { 10, 20, 30, 40, 50 };
    //int c[arraySize] = { 0 };

    //// Add vectors in parallel.
    //cudaError_t cudaStatus = addWithCuda(c, a, b, arraySize);
    //if (cudaStatus != cudaSuccess) {
    //    fprintf(stderr, "addWithCuda failed!");
    //    return 1;
    //}

    //printf("{1,2,3,4,5} + {10,20,30,40,50} = {%d,%d,%d,%d,%d}\n",
    //    c[0], c[1], c[2], c[3], c[4]);

    //// cudaDeviceReset must be called before exiting in order for profiling and
    //// tracing tools such as Nsight and Visual Profiler to show complete traces.
    //cudaStatus = cudaDeviceReset();
    //if (cudaStatus != cudaSuccess) {
    //    fprintf(stderr, "cudaDeviceReset failed!");
    //    return 1;
    //}

    //return 0;
}

// Helper function for using CUDA to add vectors in parallel.
cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size)
{
    int *dev_a = 0;
    int *dev_b = 0;
    int *dev_c = 0;
    cudaError_t cudaStatus;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    // Allocate GPU buffers for three vectors (two input, one output)    .
    cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    // Launch a kernel on the GPU with one thread for each element.
    addKernel<<<1, size>>>(dev_c, dev_a, dev_b);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_c);
    cudaFree(dev_a);
    cudaFree(dev_b);
    
    return cudaStatus;
}

cudaError_t Simpson(double a, double b, double * c, int n)
{
	return cudaError_t();
}
