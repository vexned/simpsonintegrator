#include <iostream>
#include <cmath>
#include <chrono>

using namespace std;

double f1(double x);
double f2(double x);
double f3(double x);
double Simpson(double a, double b, int N, double(*pfunc)(double));

int main() {
	int N = 1000000000;
	double x1, x2, x3;
	long long sum = 0;
	for (int i = 0; i < 10; i++) {
		auto t1 = std::chrono::high_resolution_clock::now();
		x1 = Simpson(5.2, 12.0, N, f1);
		auto t2 = std::chrono::high_resolution_clock::now();
		cout << "f1 took " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
			<< " microseconds. result = " << x1 << endl;
		sum += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	}
	cout << "Avg: " << sum / 10 << " microseconds";
	/*t1 = std::chrono::high_resolution_clock::now();
	x2 = Simpson(3.0, 7.2, N, f2);
	t2 = std::chrono::high_resolution_clock::now();
	cout << "f2 took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds. result = " << x2 << endl;
	t1 = std::chrono::high_resolution_clock::now();
	x3 = Simpson(1.2, 5.3, N, f3);
	t2 = std::chrono::high_resolution_clock::now();
	cout << "f3 took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds. result = " << x3 << endl;*/

	system("pause");
	return 0;
}


double Simpson(double a, double b, int N, double (*pfunc)(double))
{
	//���������� ��������� �� ������� �������� ������ ���� ������
	if (N & 1)
		return 0;
	double h = (b - a) / N;
	double sumEven = 0;
	double sum = 0;
	double sumOdd = 0;
	sum += (*pfunc)(a);
	for (int i = 1; i < N; i = i+2)
	{
		//��������
		sumOdd += (*pfunc)(a + i * h);
		//������
		sumEven += (*pfunc)(a + (i+1) * h);
	}

	sum += (*pfunc)(b) + sumEven *2 + sumOdd*4;
	sum = (h / 3.0)*sum;

	return sum;
}

//double Simpson(double a, double b, int N, double(*pfunc)(double))
//{
//	//���������� ��������� �� ������� �������� ������ ���� ������
//	if (N & 1)
//		return 0;
//	double h = (b - a) / N;
//
//	double sum1 = 0;
//	double sum2 = 0;
//
//	double sum = 0;
//	double sumEven = 0;
//	double sumOdd = 0;
//	sum += (*pfunc)(a);
//	for (int i = 1; i < N; i = i + 2)
//	{
//		//��������
//		sumOdd += 4 * (*pfunc)(a + i * h);
//		//������
//		sumEven += 2 * (*pfunc)(a + (i + 1) * h);
//	}
//
//	sum2 = sumEven + sumOdd;
//	sum += (*pfunc)(b) + sum2;
//	sum = (h / 3.0)*sum;
//
//	return sum;
//}

//5.2 12.0
double f1(double x) {
	return ((x + 1)*(x + 1)) / sqrt(log(x));
}


//3.0 7.2
double f2(double x) {
	return sqrt(x) * exp(-x / 2);
}


//1.2 5.3
double f3(double x) {
	return ((1 + x)*(1 + x)) / (x*x*x*sqrt(2 + x));
}