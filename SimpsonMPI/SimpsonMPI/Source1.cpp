#include<mpi.h>
#include<iostream>
#include <omp.h>
#include <conio.h>
#include <chrono>

using namespace std;

double f1(double x);
double Simpson(double a, double b, int N, double(*pfunc)(double));


int main(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	double b1 = 12.0;
	double a1 = 5.2;


	const int N = 1000000000 / size;
	double summ = 0.0;


	double buf;
	buf = 0;
	MPI_Status status;


	double startTime = MPI_Wtime();
	auto t1 = std::chrono::high_resolution_clock::now();
	int pr = size - rank;
	double part = (b1 - a1) / size;

	double summForProccess = 0;

	double l, r;
	l = a1 + (pr - 1)*part;
	r = a1 + ((pr)*part);

	summForProccess += Simpson(l, r, N, f1);

	double resSumm;
	MPI_Reduce(&summForProccess, &resSumm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	if (rank == 0) {
		auto t2 = std::chrono::high_resolution_clock::now();
		double endTime = MPI_Wtime();
		double res = resSumm;
		cout << "f1 took " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
			<< " microseconds. result = " << res << endl;

	}

	MPI_Finalize();
	system("pause");
	return 0;
}


double Simpson(double a, double b, int N, double(*pfunc)(double))
{
	//���������� ��������� �� ������� �������� ������ ���� ������
	if (N & 1)
		return 0;
	double h = (b - a) / N;
	double sumEven = 0;
	double sum = 0;
	double sumOdd = 0;
	sum += (*pfunc)(a);
	for (int i = 1; i < N; i = i + 2)
	{
		//��������
		sumOdd += (*pfunc)(a + i * h);
		//������
		sumEven += (*pfunc)(a + (i + 1) * h);
	}

	sum += (*pfunc)(b) + sumEven * 2 + sumOdd * 4;
	sum = (h / 3.0)*sum;

	return sum;
}

//5.2 12.0
double f1(double x) {
	return ((x + 1)*(x + 1)) / sqrt(log(x));
}
