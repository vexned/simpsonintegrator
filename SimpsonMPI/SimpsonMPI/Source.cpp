#include<mpi.h>
#include<iostream>
#include <omp.h>
#include <conio.h>
#include <chrono>

using namespace std;

double f1(double x);
double f2(double x);
double f3(double x);
double Simpson(double a, double b, int N, double(*pfunc)(double));

int main1(int argc, char *argv[])
{
	//double startTime = MPI_Wtime();
	MPI_Init(&argc, &argv);
	double startTime = MPI_Wtime();
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	cout << "Hello from" << rank << "of" << size << endl;


	double endTime = MPI_Wtime();
	double workTime = endTime - startTime;
	cout << rank << ":" << workTime << endl;
	MPI_Finalize();
	system("pause");
	return 0;
}


int main(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	double b1 = 12.0;
	double a1 = 5.2;

	double b2 = 7.2;
	double a2 = 3.0;

	double b3 = 5.3;
	double a3 = 1.2;


	const int N = 1000000000/size;
	double summ = 0.0;


	double buf;
	buf = 0;
	MPI_Status status;


	double startTime = MPI_Wtime();
	auto t1 = std::chrono::high_resolution_clock::now();
	int pr = size - rank;
	double part = (b1 - a1) / size;

	double summForProccess = 0;

	double l, r;
	l = a1 + (pr - 1)*part;
	r = a1 + ((pr)*part);
	//cout << l << "  " << r << endl;

	summForProccess += Simpson(l, r, N, f1);

	double resSumm;
	MPI_Reduce(&summForProccess, &resSumm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	if (rank == 0) {
		auto t2 = std::chrono::high_resolution_clock::now();
		double endTime = MPI_Wtime();
		double res = resSumm;
		cout << "f1 took " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
			<< " microseconds. result = " << res << endl;

	}

	//startTime = MPI_Wtime();
	//t1 = std::chrono::high_resolution_clock::now();
	//pr = size - rank;
	//part = (b2 - a2) / size;

	//summForProccess = 0;

	//l = a2 + (pr - 1)*part;
	//r = a2 + ((pr)*part);
	////cout << l << "  " << r << endl;

	//summForProccess += Simpson(l, r, N, f2);


	//MPI_Reduce(&summForProccess, &resSumm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	//if (rank == 0) {
	//	auto t2 = std::chrono::high_resolution_clock::now();
	//	double endTime = MPI_Wtime();
	//	double res = resSumm;
	//	cout << "f2 took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
	//		<< " milliseconds. result = " << res << endl;

	//}

	//startTime = MPI_Wtime();
	//t1 = std::chrono::high_resolution_clock::now();
	//pr = size - rank;
	//part = (b3 - a3) / size;

	//summForProccess = 0;

	//l = a3 + (pr - 1)*part;
	//r = a3 + ((pr)*part);
	////cout << l << "  " << r << endl;

	//summForProccess += Simpson(l, r, N, f3);


	//MPI_Reduce(&summForProccess, &resSumm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	//if (rank == 0) {
	//	auto t2 = std::chrono::high_resolution_clock::now();
	//	double endTime = MPI_Wtime();
	//	double res = resSumm;
	//	cout << "f3 took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
	//		<< " milliseconds. result = " << res << endl;

	//}


	MPI_Finalize();
	system("pause");
	return 0;
}


double Simpson(double a, double b, int N, double(*pfunc)(double))
{
	//���������� ��������� �� ������� �������� ������ ���� ������
	if (N & 1)
		return 0;
	double h = (b - a) / N;
	double sumEven = 0;
	double sum = 0;
	double sumOdd = 0;
	sum += (*pfunc)(a);
	for (int i = 1; i < N; i = i + 2)
	{
		//��������
		sumOdd += (*pfunc)(a + i * h);
		//������
		sumEven += (*pfunc)(a + (i + 1) * h);
	}

	sum += (*pfunc)(b) + sumEven * 2 + sumOdd * 4;
	sum = (h / 3.0)*sum;

	return sum;
}

//5.2 12.0
double f1(double x) {
	return ((x + 1)*(x + 1)) / sqrt(log(x));
}


//3.0 7.2
double f2(double x) {
	return sqrt(x) * exp(-x / 2);
}


//1.2 5.3
double f3(double x) {
	return ((1 + x)*(1 + x)) / (x*x*x*sqrt(2 + x));
}